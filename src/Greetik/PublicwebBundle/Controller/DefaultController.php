<?php

namespace Greetik\PublicwebBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    public function indexAction(Request $request) {
        try {
            $response = $this->get($this->getParameter('publicweb.service'))->sendheader(true);
        } catch (\Exception $e) {
            $response = new Response();
        }

        return $this->render($this->getParameter('publicweb.interface') . ':index.html.twig', $this->get($this->getParameter('publicweb.service'))->getParamsIndex($request), $response
        );
    }

    public function sectionAction(Request $request, $title, $id, $page = '', $category = '') {
        $section = $this->get($this->getParameter('treesections.permsservice'))->getPublicTreesection($id);
        if ($title != $this->get('beinterface.tools')->spaceencodeurl($section['title'], true)) {
            return $this->redirectToRoute('publicweb_section', array('title' => $this->get('beinterface.tools')->spaceencodeurl($section['title'], true), 'id' => $section['id'], 'category' => $category, 'page' => $page), 301);
        }

        $sendmail = $this->checksendmail($request);
        if ($sendmail)
            return $this->redirectToRoute('publicweb_sectionmailsended', array('title' => $this->get('beinterface.tools')->spaceencodeurl($section['title'], true), 'id' => $section['id'], 'category' => $category, 'page' => $page));

        try {
            $response = $this->get($this->getParameter('publicweb.service'))->sendheader(true);
        } catch (\Exception $e) {
            $response = new Response();
        }

        return $this->render($this->getParameter('publicweb.interface') . ':section.html.twig', $this->get($this->getParameter('publicweb.service'))->getParamsSection($request, $id, $page, $category), $response);
    }

    public function eventAction(Request $request, $title, $id, $page = '') {
        $event = $this->get($this->getParameter('events.permsservice'))->getPublicEvent($id, $page);

        if ($title != $this->get('beinterface.tools')->spaceencodeurl($event['title'], true)) {
            return $this->redirectToRoute('publicweb_event', array('title' => $this->get('beinterface.tools')->spaceencodeurl($event['title'], true), 'id' => $event['id']), 301);
        }

        $sendmail = $this->checksendmail($request);
        if ($sendmail)
            return $this->redirectToRoute('publicweb_eventmailsended', array('title' => $this->get('beinterface.tools')->spaceencodeurl($event['title'], true), 'id' => $event['id']));

        try {
            $response = $this->get($this->getParameter('publicweb.service'))->sendheader(true);
        } catch (\Exception $e) {
            $response = new Response();
        }

        return $this->render($this->getParameter('publicweb.interface') . ':event.html.twig', $this->get($this->getParameter('publicweb.service'))->getParamsEvent($request, $id, $page), $response);
    }

    public function postAction(Request $request, $title, $id, $page = '') {
        $post = $this->get($this->getParameter('blog.permsservice'))->getPublicPost($id, $page);

        if ($title != $this->get('beinterface.tools')->spaceencodeurl($post['title'], true)) {
            return $this->redirectToRoute('publicweb_post', array('title' => $this->get('beinterface.tools')->spaceencodeurl($post['title'], true), 'id' => $post['id']), 301);
        }

        $sendmail = $this->checksendmail($request);
        if ($sendmail)
            return $this->redirectToRoute('publicweb_postmailsended', array('title' => $this->get('beinterface.tools')->spaceencodeurl($post['title'], true), 'id' => $post['id']));

        try {
            $response = $this->get($this->getParameter('publicweb.service'))->sendheader(true);
        } catch (\Exception $e) {
            $response = new Response();
        }

        return $this->render($this->getParameter('publicweb.interface') . ':post.html.twig', $this->get($this->getParameter('publicweb.service'))->getParamsPost($request, $id, $page), $response);
    }

    public function tagAction(Request $request, $tag, $page = '') {
        $sendmail = $this->checksendmail($request);
        if ($sendmail)
            return $this->redirectToRoute('publicweb_tagmailsended', array('tag' => $tag, 'page' => $page));

        try {
            $response = $this->get($this->getParameter('publicweb.service'))->sendheader(true);
        } catch (\Exception $e) {
            $response = new Response();
        }

        return $this->render($this->getParameter('publicweb.interface') . ':tag.html.twig', $this->get($this->getParameter('publicweb.service'))->getParamsTag($request, $tag, $page), $response
        );
    }

    public function sitemapAction(Request $request) {
        $response = new Response();
        $response->headers->set('Content-Type', 'xml');

        return $this->render('PublicwebBundle:Default:sitemap.xml.twig', array(
                    'urls' => $this->get($this->getParameter('publicweb.service'))->getSitemap(ceil($this->get('dataimage.tools')->findTotalElems('image') / 1000)),
                    'hostname' => $request->getSchemeAndHttpHost()
                        ), $response);
    }

    public function sitemapimagesAction(Request $request, $page = '') {
        $response = new Response();
        $response->headers->set('Content-Type', 'xml');

        return $this->render('PublicwebBundle:Default:sitemapimages.xml.twig', array(
                    'urls' => $this->get($this->getParameter('publicweb.service'))->getSitemap(0, true, $page),
                    'hostname' => $request->getSchemeAndHttpHost()
                        ), $response);
    }

    public function geositemapAction(Request $request) {
        $response = new Response();
        $response->headers->set('Content-Type', 'xml; charset="UTF-8"');

        return $this->render('PublicwebBundle:Default:geositemap.kml.twig', array(
                    'title' => $this->getParameter('projectname'),
                    'hostname' => $request->getSchemeAndHttpHost(),
                    'placemarks' => array(array('title' => $this->getParameter('projectname'), 'address' => $this->getParameter('projectaddress'), 'description' => $this->getParameter('projectdescription'), 'coordinates' => $this->getParameter('projectlat') . ',' . $this->getParameter('projectlon') . ',0'))
                        ), $response);
    }

    public function rssAction(Request $request) {
        $response = new Response();
        $response->headers->set('Content-Type', 'xml');

        return $this->render('PublicwebBundle:Default:rss.xml.twig', array(
                    'data' => $this->get($this->getParameter('publicweb.service'))->getRss(),
                    'hostname' => $request->getSchemeAndHttpHost(),
                    'title' => $this->getParameter('projectname'),
                    'description' => $this->getParameter('projectdescription'),
                    'generator' => $request->getSchemeAndHttpHost()
                        ), $response);
    }

    public function robotsAction(Request $request) {
        $response = new Response();
        $response->headers->set('Content-Type', 'text/plain; charset="UTF-8"');

        return $this->render('PublicwebBundle:Default:robots.txt.twig', array(
                    'totalimages' => $this->get('dataimage.tools')->findTotalElems('image'),
                    'numimages' => ceil($this->get('dataimage.tools')->findTotalElems('image') / 1000),
                    'hostname' => $request->getSchemeAndHttpHost(),
                        ), $response);
    }

    protected function checksendmail($request) {
        if ($request->getMethod() == 'POST') {
            if ($request->get('sendmail')) {
                try {
                    $this->get('webforms.tools')->sendmail($request->get('sendmail'), $request, true);
                    $this->addFlash('success', 'Email enviado Correctamente');
                    return true;
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return false;
    }

    public function getmarkersAction($idproject) {
        return new Response(json_encode(array('errorCode' => 0, 'data' => $this->get($this->getParameter('publicweb.service'))->getMap($idproject)['markers'])), 200, array('Content-Type' => 'application/json'));
    }

    public function getgpxsAction($idproject) {
        return new Response(json_encode(array('errorCode' => 0, 'data' => $this->get($this->getParameter('publicweb.service'))->getMap($idproject)['gpx'])), 200, array('Content-Type' => 'application/json'));
    }

    public function productAction(Request $request, $title, $id) {
        $product = $this->get($this->getParameter('catalog.permsservice'))->getPublicProduct($id);

        if ($title != $this->get('beinterface.tools')->spaceencodeurl($product['mainfield'], true)) {
            return $this->redirectToRoute('publicweb_product', array('title' => $this->get('beinterface.tools')->spaceencodeurl($product['mainfield'], true), 'id' => $product['id']), 301);
        }

        $sendmail = $this->checksendmail($request);
        if ($sendmail)
            return $this->redirectToRoute('publicweb_productmailsended', array('title' => $this->get('beinterface.tools')->spaceencodeurl($product['mainfield'], true), 'id' => $product['id']));

        try {
            $response = $this->get($this->getParameter('publicweb.service'))->sendheader(true);
        } catch (\Exception $e) {
            $response = new Response();
        }

        return $this->render($this->getParameter('publicweb.interface') . ':product.html.twig', $this->get($this->getParameter('publicweb.service'))->getParamsProduct($request, $id), $response);
    }

}
