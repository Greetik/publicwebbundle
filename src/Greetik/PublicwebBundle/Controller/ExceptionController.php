<?php
namespace Greetik\PublicwebBundle\Controller;

use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController as BaseExceptionController;
use Symfony\Component\HttpFoundation\Request;

class ExceptionController extends BaseExceptionController
{
    
    private $interface;
    private $container;
    private $service;
    
    public function __construct(\Twig_Environment $twig, $debug, $interface, $service, $container)
    {
        $this->twig = $twig;
        $this->debug = $debug;
        $this->interface=$interface;
        $this->container = $container;
        $this->service = $service;
    }
    
    public function showAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = NULL)
    {
        $code = $exception->getStatusCode();
        if ($code == '404' || $code == '403'){
            return new Response($this->twig->render($this->interface.':'.$code.'.html.twig', $this->container->get($this->service)->getParamsCommon()));        
        }

        return parent::showAction($request, $exception, $logger);
    }
}