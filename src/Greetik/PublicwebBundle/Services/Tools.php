<?php

namespace Greetik\PublicwebBundle\Services;

use Symfony\Component\HttpFoundation\Response;
use Greetik\WebmodulesBundle\DBAL\Types\WebmoduleType;
use Abraham\TwitterOAuth\TwitterOAuth;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Tools {

    private $posts;
    private $webforms;
    private $sections;
    private $maps;
    private $catalog;
    private $mailer;
    private $rootdir;
    private $router;
    private $beinterface;
    private $tw_consumerkey;
    private $tw_consumersecret;
    private $tw_screenname;

    public function __construct($_posts, $_webforms, $_sections, $_maps, $_catalog, $_mailer, $_router, $_beinterface, $_rootdir, $_tw_conkey, $_tw_conse, $_tw_name) {
        $this->posts = $_posts;
        $this->webforms = $_webforms;
        $this->sections = $_sections;
        $this->maps = $_maps;
        $this->catalog = $_catalog;
        $this->mailer = $_mailer;
        $this->rootdir = $_rootdir;
        $this->router = $_router;
        $this->beinterface = $_beinterface;
        $this->tw_consumerkey = $_tw_conkey;
        $this->tw_consumersecret = $_tw_conse;
        $this->tw_screenname = $_tw_name;
    }

    function getFirstImageSection($id, $onlyurl = false) {
        $section = $this->sections->getPublicTreesection($id);
        if (isset($section['images'][0])) {
            if ($onlyurl)
                return $section['images'][0]['url'];
            return $section['images'][0];
        }
        return '';
    }

    public function getRss() {
        $data = array();

        //posts
        $posts = $this->posts->getPublishedPostsByPage('', 1, 30);
        foreach ($posts as $post) {
            $data[] = array(
                'link' => $this->router->generate('publicweb_post', array('title' => $this->beinterface->spaceencodeurl($post['title'], true), 'id' => $post['id'])),
                'title' => $post['title'],
                'body' => $post['body'],
                'attime' => $post['publishdate']->format('Y-m-d')
            );
        }

        return $data;
    }

    public function getBreadcrumb($treesection, $category = '') {
        if (is_numeric($treesection))
            $treesection = $this->sections->getTreesection($treesection);

        $pathcategory = array();
        if (!empty($category)) {
            if (is_numeric($category))
                $category = $this->sections->getTreesection($category);


            $pathcategory = $this->sections->getPath($category);
            foreach ($pathcategory as $k => $v)
                $v->setExtra('isacatalog');
        }

        return array_merge($this->sections->getPath($treesection), $pathcategory);
    }

    public function getSection($treesection, $page = '', $simple = false, $category = '', $orderby = '') {
        if (is_numeric($treesection))
            $treesection = $this->sections->getTreesection($id);
        if ($simple)
            return $treesection;

        foreach ($treesection['modules'] as $module) {
            switch ($module['moduledata']['moduletype']) {
                case WebmoduleType::BLOG:
                    if (empty($page))
                        $page = 1;
                    $numpages = ceil($this->posts->getNumberOfPostsByProject($module['module']) / 5);
                    $treesection['insertmodules'][$module['module']] = $this->posts->getPublishedPostsByPage($module['module'], $page, 5);
                    break;
                case WebmoduleType::TREESECTION: $treesection['insertmodules'][$module['module']] = $this->sections->getAllTreesectionsByProject($module['module']);
                    break;
                case WebmoduleType::WEBFORMS: $treesection['insertmodules'][$module['module']] = $this->webforms->getFormfieldsByProject($module['module']);
                    break;
                case WebmoduleType::MAP: $treesection['insertmodules'][$module['module']] = $this->maps->getMap($module['module']);
                    break;
                case WebmoduleType::CATALOG:
                    $categories = array($category);
                    if (!empty($category)) {
                        $cat = $this->sections->getAllChildrenOfBranch($category);
                        foreach ($cat as $k => $v)
                            $categories[] = $v['id'];
                    }

                    $tree = $this->sections->getTreesectionsByProject($this->catalog->getCatalogCategoriesId($module['module']), $category);
                    $treesection['insertmodules'][$module['module']] = array(
                        'products' => $this->catalog->getProductsByProject($module['module'], $categories, $orderby),
                        'categories' => $tree
                    );
                    break;
            }
        }

        return array($treesection, isset($numpages) ? $numpages : 0, $page);
    }

    public function sendheader($nocache = false) {
        if (!$nocache) {
            ini_set('realpath_cache_size', '4096K');
            ini_set('realpath_cache_ttl', 7200);
            $expires = 364 * 60 * 60 * 24; // how long to cache in secs..
        } else
            $expires = 60; // how long to cache in secs..

        $response = new Response();
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'public, maxage=' . $expires);
        $response->headers->set('Expires', gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');
        //$response->send();
        return $response;
    }

    public function getLasttweets($number = 4) {
        $path = '../var/twitter/';
        $configfile = 'lastchecked.json';
        if (!file_exists($path))
            mkdir($path);

        if (file_exists($path . $configfile))
            $config = json_decode(file_get_contents($path . $configfile), 1);
        else
            $config = array('lasttweetscheck' => date('Y-m-d', time() - 7 * 24 * 60 * 60));


        $lasttimechecked = $config['lasttweetscheck'];
        if ($lasttimechecked) {
            $lasttimechecked = new \Datetime($lasttimechecked);
            if ((int) $lasttimechecked->diff(new \Datetime())->format("%r%h") + intval($lasttimechecked->diff(new \Datetime())->format("%R%a")) * 24 <= 12)
                return $config['lasttweets'];
        }

        $connection = new TwitterOAuth($this->tw_consumerkey, $this->tw_consumersecret);
        $lasttweets = $connection->get('statuses/user_timeline', array('count' => $number, 'screen_name' => 'greetik_'));

        $config['lasttweetscheck'] = date('Y-m-d H:i:s');
        $config['lasttweets'] = $lasttweets;
        $file = fopen($path . $configfile, 'w+');
        fwrite($file, json_encode($config));
        fclose($file);

        return $lasttweets;
    }

    public function getSitemap($treesections, $posts, $tags, $products, $categories, $numsitemapimages = 0, $forimages = false, $sitemappage = 0) {
        $urls = array();

        if (!$forimages) {
            //sitemapimages
            for ($i = 1; $i <= $numsitemapimages; $i++) {
                $urls[] = array(
                    'loc' => $i == 1 ? $this->router->generate('publicweb_sitemapimages') : $this->router->generate('publicweb_sitemapimagespage', array('page' => $i)),
                    'changefreq' => 'monthly',
                    'priority' => '0.9'
                );
            }

            // index
            $urls[] = array(
                'loc' => $this->router->generate('publicweb_homepage'),
                'changefreq' => 'monthly',
                'priority' => '0.9'
            );

            // locations
            $urls[] = array(
                'loc' => $this->router->generate('publicweb_geositemap'),
                'changefreq' => 'never',
                'priority' => '0.9'
            );
        }

        //sections
        foreach ($treesections as $section) {
            $numimages = 0;
            $this->sitemapsection($section, $urls, $forimages, $sitemappage, $numimages);
        }

        //posts
        //$numpages = intval($this->posts->getAmountOfPublishedPostsByProject($module['module']) / 5);
        //$item = $this->getPublishedpostsByProject($module['module'], $page, 5);
        foreach ($posts as $post) {
            if (!$forimages) {
                $urls[] = array(
                    'loc' => $this->router->generate('publicweb_post', array('title' => $this->beinterface->spaceencodeurl($post['title'], true), 'id' => $post['id'])),
                    'changefreq' => 'never',
                    'priority' => '0.8'
                );
            } else {

                if (count($post['images']) > 0) {
                    $arrayurl = array(
                        'loc' => $this->router->generate('publicweb_post', array('title' => $this->beinterface->spaceencodeurl($post['title'], true), 'id' => $post['id'])),
                        'images' => array()
                    );

                    foreach ($post['images'] as $image) {
                        $titleimage = $image['name'] != '' ? $image['name'] : $post['title'];
                        array_push($arrayurl['images'], array('loc' => $image['url'], 'caption' => $image['name'] . '' . $image['comments'], 'title' => $titleimage));
                        $numimages++;
                    }

                    if ($sitemappage > 0) {
                        if ($numimages >= $sitemappage * 1000)
                            return $urls;
                        else if ($numimages < ($sitemappage - 1) * 1000)
                            continue;
                    }
                    $urls[] = $arrayurl;
                }
            }
        }

        //products
        foreach ($products as $product) {
            if (!$forimages) {
                $urls[] = array(
                    'loc' => $this->router->generate('publicweb_product', array('title' => $this->beinterface->spaceencodeurl($product['mainfield'], true), 'id' => $product['id'])),
                    'changefreq' => 'never',
                    'priority' => '0.9'
                );
            } else {
                if (count($product['images']) > 0) {
                    $arrayurl = array(
                        'loc' => $this->router->generate('publicweb_post', array('title' => $this->beinterface->spaceencodeurl($product['mainfield'], true), 'id' => $product['id'])),
                        'images' => array()
                    );

                    foreach ($product['images'] as $image) {
                        $titleimage = $image['name'] != '' ? $image['name'] : $post['title'];
                        array_push($arrayurl['images'], array('loc' => $image['url'], 'caption' => $image['name'] . '' . $image['comments'], 'title' => $titleimage));
                        $numimages++;
                    }

                    if ($sitemappage > 0) {
                        if ($numimages >= $sitemappage * 1000)
                            return $urls;
                        else if ($numimages < ($sitemappage - 1) * 1000)
                            continue;
                    }
                    $urls[] = $arrayurl;
                }
            }
        }

        //categories
        foreach ($categories as $cat) {
            foreach ($cat['categories'] as $k => $category){
                $urls[] = $this->sitemapcategory($cat['section'], $category['id']);
            }
        }

        if (!$forimages) {
            //tags
            foreach ($tags as $tag) {
                $numpages = intval($this->posts->getNumberOfPostsByTag(urldecode($tag['name'])) / 5);
                for ($i = 0; $i < $numpages; $i++) {
                    if ($i > 0)
                        $datapage = array('page' => $i + 1);
                    else
                        $datapage = array();

                    $urls[] = array(
                        'loc' => $this->router->generate('publicweb_tag', array_merge($datapage, array('tag' => urlencode($tag['name'])))),
                        'changefreq' => 'weekly',
                        'priority' => '0.7'
                    );
                }
            }
        }

        return $urls;
    }

    protected function sitemapcategory($section, $category) {
        return array(
            'loc' => $this->router->generate('publicweb_section', array('title' => $this->beinterface->spaceencodeurl($section['title'], true), 'id' => $section['id'], 'category'=>$category)),
            'changefreq' => 'yearly',
            'priority' => '0.8'
        );
    }

    protected function sitemapsection($section, &$urls, $forimages, $sitemappage, &$numimages = 0) {
        if (!$forimages) {
            if (isset($section['numpages']))
                $numpages = $section['numpages'];
            else
                $numpages = 1;
            $numpages = $numpages <= 0 ? 1 : $numpages;

            for ($i = 0; $i < $numpages; $i++) {
                if ($i > 0)
                    $datapage = array('page' => $i + 1);
                else
                    $datapage = array();

                $urls[] = array(
                    'loc' => $this->router->generate('publicweb_section', array_merge($datapage, array('title' => $this->beinterface->spaceencodeurl($section['title'], true), 'id' => $section['id']))),
                    'changefreq' => 'yearly',
                    'priority' => '0.7'
                );
            }
        }else {
            if (count($section['images']) > 0) {
                $arrayurl = array(
                    'loc' => $this->router->generate('publicweb_section', array('title' => $this->beinterface->spaceencodeurl($section['title'], true), 'id' => $section['id'])),
                    'images' => array()
                );

                foreach ($section['images'] as $image) {
                    $titleimage = $image['name'] != '' ? $image['name'] : $section['title'];
                    array_push($arrayurl['images'], array('loc' => $image['url'], 'caption' => $image['name'] . '' . $image['comments'], 'title' => $image['name']));
                    $numimages++;
                }

                if ($sitemappage > 0) {
                    if ($numimages < $sitemappage * 1000 && ($numimages > ($sitemappage - 1) * 1000))
                        $urls[] = $arrayurl;
                } else
                    $urls[] = $arrayurl;
            }
        }


        if (count($section['children'])) {
            foreach ($section['children'] as $child) {
                $this->sitemapsection($child, $urls, $forimages, $sitemappage, $numimages);
            }
        }
    }

}
